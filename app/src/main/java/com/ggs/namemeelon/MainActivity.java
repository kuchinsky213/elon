package com.ggs.namemeelon;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import com.sdsmdg.tastytoast.TastyToast;

import java.util.Locale;

import soup.neumorphism.NeumorphButton;

public class MainActivity extends AppCompatActivity {
    LottieAnimationView elon;
    EditText et;
    TextView rez,t1,t2,t3,t4,t5;
    NeumorphButton check;

    LinearLayout linear;
    String line;
    String newString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        elon = findViewById(R.id.elonA);
        et = findViewById(R.id.editText);
        rez = findViewById(R.id.itog);
        check = findViewById(R.id.check);
        linear = findViewById(R.id.linear);

        t1 = findViewById(R.id.t1);







        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                //      mBackground.setBackgroundResource(R.drawable.sun);
//                coordinatorLayout.setBackgroundResource(R.drawable.bg3);
//                incl.setVisibility(View.VISIBLE);
//                fab.setVisibility(View.VISIBLE);
                elon.setVisibility(View.GONE);
                linear.setVisibility(View.VISIBLE);

            }
        }, 5125); //specify the number of milliseconds








  check.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
          line = (et.getText()).toString().toLowerCase();

          newString = line.toLowerCase(Locale.ENGLISH);


//          line.replace('e', '\\u039e');
//          line.replace('i', 'ι');
//          line.replace('n', 'π');
//          line.replace('o', 'Φ');
//          line.replace('u', 'Ψ');
//          line.replace('z', '2');


//          String pattern = "(?iU)[дd][\\W_]?[уy][\\W_]?[рpr][\\W_]?[аa][\\W_]?[кk]";
//          String p ="***";
//          String newString = line.replaceAll(pattern, p);
//          System.out.println(newString);




          if(et.length()>=1 && et.length()<=15){

            rez.setVisibility(View.VISIBLE);

//              for (int i = 0; i < chars.length; i++) {
//                  if (i == 0 || chars[i - 1] == ' ') {
//
//
//
//                    rez.setText(newString);
//                  }
//              }



//                    t1.setText("");
//                    t2.setText("");
//                    t3.setText("");
//                    t4.setText("");
//                    t5.setText("");
                    String pattern = "[e][\\W_]?";
                    String pattern1 = "[i][\\W_]?";
                    String pattern2 = "[n][\\W_]?";
                    String pattern3 = "[o][\\W_]?";
                    String pattern4 = "[u][\\W_]?";
                    String pattern5 = "[z][\\W_]?";
                    String pattern6 = "[a][\\W_]?";


                    String p ="\u039e";
                    String p1 ="\u03b9";
                    String p2 ="\u03c0";
                    String p3 ="\u03a6";
                    String p4 ="\u03a8";
                    String p5 ="2";
                    String p6 ="\u03bb";


              newString = newString.replace("hristop","HRιSTΦP");
              newString = newString.replace("awre"," λWRΞ");
              newString = newString.replace("easa"," ÆSλ");
              newString = newString.replace("enja"," ΞπJλ");
              newString = newString.replace("lexa"," LΞXλ");
              newString = newString.replace("rist"," RιST");
              newString = newString.replace("ony","O-NY");
              newString = newString.replace("ond","O-ND");
              newString = newString.replace("vid"," V-1D");
              newString = newString.replace("vel"," V-3L");
              newString = newString.replace("ard"," A-RD");
              newString = newString.replace("har"," H"+"\u03bb"+"R");
              newString = newString.replace("eph"," E-PH");
              newString = newString.replace("eth"," E-7H");
              newString = newString.replace("nth"," πTH");
              newString = newString.replace("nio"," N-10");
              newString = newString.replace("les","L-35");
              newString = newString.replace("las","L-45");
              newString = newString.replace("lia","L-14");
              newString = newString.replace("los","L-05");
              newString = newString.replace("lph","L-PH");
              newString = newString.replace("ael"," A-3L");
              newString = newString.replace("nis"," N-15");
              newString = newString.replace("iam"," I-4M");
              newString = newString.replace("mas"," M-45");
              newString = newString.replace("her"," H-3R");
              newString = newString.replace("iel","I-3L");
              newString = newString.replace("ven"," V-3N");
              newString = newString.replace("son"," S-0N");
              newString = newString.replace("att"," λTT");
              newString = newString.replace("atr"," λTR");
              newString = newString.replace("rry"," R-RY");
              newString = newString.replace("rey"," R-3Y");
              newString = newString.replace("rew"," R-3W");
              newString = newString.replace("reg"," RΞG");
              newString = newString.replace("ank"," A-NK");
              newString = newString.replace("ark"," λRC");
              newString = newString.replace("aym"," λΨM");
              newString = newString.replace("ott"," O-77");
              newString = newString.replace("ory"," O-RY");
              newString = newString.replace("oug"," OUG");
              newString = newString.replace("ick"," I-CK");
              newString = newString.replace("ith"," I-7H");
              newString = newString.replace("hen"," H-3N");
              newString = newString.replace("hua"," H-U4");
              newString = newString.replace("hur"," H-UR");
              newString = newString.replace("rge"," R-63");
              newString = newString.replace("ria"," R-14");
              newString = newString.replace("tep"," TΞP");
              newString = newString.replace("aul"," Ꜷ-L");
              newString = newString.replace("ald"," A-LD");
              newString = newString.replace("uan"," UA-N");
              newString = newString.replace("uss"," ΨSS");
              newString = newString.replace("nto"," πTΦ");
              newString = newString.replace("nce"," N-C3");
              newString = newString.replace("lly"," L-LY");
              newString = newString.replace("eve"," E-V3");
              newString = newString.replace("sha"," S-H4");
              newString = newString.replace("ndr"," N-DR");
              newString = newString.replace("hka"," H-K4");
              newString = newString.replace("mon"," M-0N");
              newString = newString.replace("imu"," ιMΨ");
              newString = newString.replace("tia"," T-14");
              newString = newString.replace("ina"," I-N4");
              newString = newString.replace("ula"," U-L4");


              newString = newString.replace("rt","-R7");
              newString = newString.replace("ck"," C-K");
              newString = newString.replace("os"," ΦS");
              newString = newString.replace("oe"," Φ-3");
              newString = newString.replace("on"," Φπ");
              newString = newString.replace("ob"," ΦB");
              newString = newString.replace("am"," \u03bb"+"-M");
              newString = newString.replace("as"," λS");
              newString = newString.replace("ho"," HΦ");
              newString = newString.replace("an"," λπ");
              newString = newString.replace("av"," λV");
              newString = newString.replace("rk"," R-K");
              newString = newString.replace("eo","\uAB41");
              newString = newString.replace("te"," TΞ");
              newString = newString.replace("ic"," ι-C");
              newString = newString.replace("ia"," ι-4");
              newString = newString.replace("ja"," J-4");
              newString = newString.replace("nd"," πD");
              newString = newString.replace("en"," Ξπ");
              newString = newString.replace("er"," ΞR");
              newString = newString.replace("ex"," Ξ-X");
              newString = newString.replace("rl"," R-L");
              newString = newString.replace("oy"," Φ-Y");
              newString = newString.replace("dd"," D-D");
              newString = newString.replace("ya"," Ψ-4");
              newString = newString.replace("ea"," "+"\u00c6");



                    newString = newString.replaceAll(pattern, p);
                    newString = newString.replaceAll(pattern1, p1);
                    newString = newString.replaceAll(pattern2, p2);
                    newString = newString.replaceAll(pattern3, p3);
                    newString = newString.replaceAll(pattern4, p4);
                    newString = newString.replaceAll(pattern5, p5);
                    newString = newString.replaceAll(pattern6, p6);





          //    String pattern6 = "[james][\\W_]?";




//              char[] chars = newString.toCharArray();
//              for (int i = 0; i < chars.length; i++) {
//
//              }

                String text = "";
              String result = newString.replaceAll("(.)\\s*(\\S.{3})$", "$1 $2");

              System.out.println(newString);

              if (line.contains("c")|| line.contains("C")){
                  text= text +("\n" +
                          "C: chemical element with symbol C and atomic number 6; common element of all known life");

              }
              if (line.contains("v")|| line.contains("V")){
                  text= text +("\n" +
                          "V: natural number");

              }              if (line.contains("q")|| line.contains("Q")){
                  text= text +("\n" +
                          "Q: chemical compound");

              }
              if (line.contains("l")|| line.contains("L")){
                  text= text +("\n" +
                          "L: unit of volume accepted for use with the SI");

              }

              if (line.contains("e")|| line.contains("E")){
                  text= text +("\n" +
                          "E: one of the four cardinal directions");

              }

              if (line.contains("Ξ")|| line.contains("\u039e")){
                  text= text +("\n" +
                          "Ξ: letter in the Greek alphabet");

              }
              if (line.contains("m")||line.contains("M")){
                  text= text +("\n" +
                          " M: SI unit of length");
              }   if (line.contains("s")||line.contains("S")){
                  text= text +("\n" +
                          " S: day of the week");
              }
            if (line.contains("t")||line.contains("T")){
                  text= text +("\n" +
                          " T: day of the week");
              }

            if (line.contains("p")||line.contains("P")){
                  text= text +("\n" +
                          " P: chemical element with symbol P and atomic number 15");
              }
              if (line.contains("r")||line.contains("R")){
                  text= text +("\n" +
                          " R: The electrical resistance variable");
              }
              if (line.contains("f")||line.contains("F")){
                  text= text +("\n" +
                          " F: day of the week");
              }


              if (line.contains("d")||line.contains("D")){
                  text= text +("\n" +
                          " D: unit of time lasting 24 hours, derived from the period of Earth's rotation about its axis");
              }

              if (line.contains("w")|| line.contains("W")) {
                  text = text + ("\n" +
                          " W: capital of and state in Austria");
              }

              if (line.contains("o")||line.contains("O")){
                  text= text +("\n" +
                          " Φ: surface integral of the magnetic flux density");
              }
              if (line.contains("j")||line.contains("J")){
                  text= text +("\n" +
                          " J: fifth planet from the Sun and largest planet in the Solar System");
              }
     if (line.contains("g")||line.contains("G")){
                  text= text +("\n" +
                          " G: unit of mass 1/1000th of a kilogram");
              }

     if (line.contains("k")||line.contains("K")){
                  text= text +("\n" +
                          " K: SI unit of thermodynamic temperature");
              }     if (line.contains("l")||line.contains("L")){
                  text= text +("\n" +
                          " L: unit of volume accepted for use with the SI");
              }




            //--------------------------sochetaniya


              if (line.contains("ael")||line.contains("Ael")){
                 text= text +("\n" +" A-3L: Organization");
              }
              if (line.contains("easa")||line.contains("Aesa")){
                 text= text +("\n" +" ÆSλ: type of phased array radar");
              }
              if (line.contains("ula")||line.contains("Ula")){
                 text= text +("\n" +" U-L4: scientific article");
              }

              if (line.contains("imu")||line.contains("Imu")){
                 text= text +("\n" +" ιMΨ: Private research university in Malaysia");
              }
                            if (line.contains("rist")||line.contains("Rist")){
                 text= text +("\n" +" RιST: family name");
              }

                            if (line.contains("ina")||line.contains("Ina")){
                 text= text +("\n" +" I-N4: gene in the species Mus musculus");
              }


              if (line.contains("sha")||line.contains("Sha")){
                 text= text +("\n" +" S-H4: cell line");
              }              if (line.contains("av")||line.contains("Av")){
                 text= text +("\n" +" λV: Swedish DJ and musician (1989-2018)");
              }
              if (line.contains("as")||line.contains("As")){
                 text= text +("\n" +" λS: scientific article published in Physical Review Letters");
              }

              if (line.contains("ya")||line.contains("Ya")){
                 text= text +("\n" +" Ψ-4: rapid transit line in Berlin, Germany");
              }


              if (line.contains("av")||line.contains("Av")){
                 text= text +("\n" +" λV: Swedish DJ and musician (1989-2018)");
              }

                            if (line.contains("ea")||line.contains("Ea")){
                 text= text +("\n" +" Æ: Elven spelling of Ai (love &/or Artificial intelligence)");
              }

                            if (line.contains("arc")||line.contains("Arc")){
                 text= text +("\n" +" λRC: language belonging to the Semitic family, part of the Northwest Semitic subfamily");
              }




                            if (line.contains("tia")||line.contains("Tia")){
                 text= text +("\n" +" T-14: Russian main battle tank");
              }


              if (line.contains("hka")||line.contains("Hka")){
                 text= text +("\n" +" H-K4: mammalian protein found in Homo sapiens");
              }
              if (line.contains("ia")||line.contains("Ia")){
                 text= text +("\n" +" ι-4: inline piston engine with four cylinders");
              }
              if (line.contains("ja")||line.contains("Ja")){
                 text= text +("\n" +" J-4: attack aircraft by Junkers");
              }

              if (line.contains("eve")||line.contains("Eve")){
                 text= text +("\n" +" E-V3: human Y-chromosome DNA haplogroup");
              }
              if (line.contains("ria")||line.contains("Ria")){
                 text= text +("\n" +" R-14: radial piston engine configuration");
              }              if (line.contains("mon")||line.contains("Mon")){
                 text= text +("\n" +" M-0N: firewall distribution");
              }

              if (line.contains("lexa")||line.contains("Lexa")){
                 text= text +("\n" +" LΞXλ: Brazilian singer-songwriter, actor and singer");
              }
                                  if (line.contains("ex")||line.contains("Ex")){
                 text= text +("\n" +" Ξ-X: letter of the Latin alphabet");
              }
                            if (line.contains("ndr")||line.contains("Ndr")){
                 text= text +("\n" +" N-DR: scientific article published on November 2003");
              }

              if (line.contains("lly")||line.contains("Lly")){
                 text= text +("\n" +" L-LY: Catalysis of the reaction: ATP + L-lysine + tRNA(Lys) = AMP + diphosphate + L-lysyl-tRNA(Lys).");
              }
              if (line.contains("awre")||line.contains("Awre")){
                 text= text +("\n" +" λWRΞ: village and civil parish in Gloucestershire, United Kingdom");
              }

              if (line.contains("enja")||line.contains("Enja")){
                 text= text +("\n" +" ΞπJλ: aerodrome in Jan Mayen, Norway");
              }

              if (line.contains("lph")||line.contains("Lph")){
                 text= text +("\n" +" L-PH: chemical compound");
              }
              if (line.contains("vel")||line.contains("Vel")){
                 text= text +("\n" +" V-3L: Wikimedia list article");
              }

              if (line.contains("los")||line.contains("Los")){
                 text= text +("\n" +" L-05: Japanese data communication device");
              }

                            if (line.contains("nio")||line.contains("Nio")){
                 text= text +("\n" +" N-10: road in France");
              }

                            if (line.contains("nto")||line.contains("Nto")){
                 text= text +("\n" +" πTΦ: chemical compound");
              }




              if (line.contains("uss")||line.contains("Uss")){
                 text= text +("\n" +" ΨSS: Wikimedia disambiguation page");
              }
              if (line.contains("ank")||line.contains("Ank")){
                 text= text +("\n" +" A-NK: scientific article published on 14 May 2015");
              }
              if (line.contains("min")||line.contains("Min")){
                 text= text +("\n" +" M-1N: Mina Luna Vincent is a Mexican-Australian Director, fashion designer and producer. She is best known for producing the first fashion film with augmented reality in history. Mina Luna incorporate dynamic characters that portray the femme fatale women");
              }


              if (line.contains("ic")||line.contains("Ic")){
                 text= text +("\n" +" ι-C: star in the constellation Cancer");
              }

              if (line.contains("dd")||line.contains("Dd")){
                 text= text +("\n" +" D-D: chemical compound");
              }
              if (line.contains("oy")||line.contains("Oy")){
                 text= text +("\n" +" Φ-Y: type of business entity in Finland");
              }

              if (line.contains("nce")||line.contains("Nce")){
                 text= text +("\n" +" N-C3: chemical compound");
              }

              if (line.contains("ck")||line.contains("Ck")){
                 text= text +("\n" +" C-K: American neuroscientist");
              }

              if (line.contains("er")||line.contains("Er")){
                 text= text +("\n" +" ΞR: sovereign state in the Horn of Africa");
              }


                            if (line.contains("oe")||line.contains("Oe")){
                 text= text +("\n" +" Φ-3: chemical compound");
              }

                            if (line.contains("uan")||line.contains("Uan")){
                 text= text +("\n" +" UA-N: movie theater in Ridgeland, Mississippi, United States");
              }



                            if (line.contains("hur")||line.contains("Hur")){
                 text= text +("\n" +" H-UR: star in the constellation Ursa Major");
              }


                            if (line.contains("oug")||line.contains("Oug")){
                 text= text +("\n" +" OUG: airport in Burkina Faso");
              }
                            if (line.contains("rl")||line.contains("Rl")){
                 text= text +("\n" +" R-L: protein-coding gene in the species Drosophila melanogaster");
              }

                            if (line.contains("las")||line.contains("Las")){
                 text= text +("\n" +" L-45: American reconnaissance satellite");
              }


                            if (line.contains("ick")||line.contains("Ick")){
                 text= text +("\n" +" I-CK: protein-coding gene in the species Homo sapiens");
              }


              if (line.contains("en")||line.contains("En")){
                 text= text +("\n" +" Ξπ: West Germanic language originating in England");
              }
              if (line.contains("atr")||line.contains("Atr")){
                 text= text +("\n" +" λTR: aircraft manufacturer");
              }

              if (line.contains("nd")||line.contains("Nd")){
                 text= text +("\n" +" πD: state of the United States of America");
              }
              if (line.contains("nis")||line.contains("Nis")){
                 text= text +("\n" +" N-15: road in France");
              }

              if (line.contains("te")||line.contains("Te")){
                  text= text +("\n" +" TΞ: Dravidian language");
              }
              if (line.contains("ven")||line.contains("Ven")){
                  text= text +("\n" +" V-3N: scientific article");
              }
              if (line.contains("eff")||line.contains("Eff")){
                  text= text +("\n" +" ΞFF: The Economic Freedom Fighters (EFF) is a radical and militant economic emancipation movement, formed in the year 2013 with the aim of bringing together revolutionary, militant activists.");
              }
              if (line.contains("hua")||line.contains("Hua")){
                  text= text +("\n" +" H-U4: cell line");
              }

   if (line.contains("dw")||line.contains("Dw")){
                  text= text +("\n" +" DW: international German public radio and television channel");
              }
   if (line.contains("rey")||line.contains("Rey")){
                  text= text +("\n" +" R-3Y: flying boat family by Convair");
              }
   if (line.contains("ard")||line.contains("Ard")){
                  text= text +("\n" +" A-RD: joint organization of Germany's regional public-service broadcasters");
              }
      if (line.contains("aym")||line.contains("Aym")){
                  text= text +("\n" +" λΨM: public university in Auburn, Alabama, United States");
              }
      if (line.contains("ond")||line.contains("Ond")){
                  text= text +("\n" +" O-ND: scientific article published in November 1964");
              }
      if (line.contains("reg")||line.contains("Reg")){
                  text= text +("\n" +" RΞG: given name");
              }

      if (line.contains("ory")||line.contains("Ory")){
                  text= text +("\n" +" O-RY: family name");
              }


   if (line.contains("nth")||line.contains("Nth")){
                  text= text +("\n" +" πTH: former science institute in Trondheim, Norway");
              }
   if (line.contains("ony")||line.contains("Ony")){
                  text= text +("\n" +" O-NY: species of virus");
              }

   if (line.contains("att")||line.contains("Att")){
                  text= text +("\n" +" λTT: American multinational conglomerate");
              }
   if (line.contains("rry")||line.contains("Rry")){
                  text= text +("\n" +" R-RY: researcher");
              }
   if (line.contains("tep")||line.contains("Tep")){
                  text= text +("\n" +" TΞP: chemical compound");
              }

   if (line.contains("hen")||line.contains("Hen")){
                  text= text +("\n" +" H-3N: cell line");
              }










              if (line.contains("aul")||line.contains("Aul")){
                 text= text +("\n" +"Ꜷ-L: municipality in Switzerland");
              }
                if (line.contains("enn")||line.contains("Enn")){
                 text= text +("\n" +"Ξππ: male given name");
              }
      if (line.contains("eth")||line.contains("Eth")){
                 text= text +("\n" +"E-7H: Tamil science reality show");
              }

                if (line.contains("rk")||line.contains("Rk")){
                 text= text +("\n" +"R-K: inverse of the quantum of conductance in the integer quantum Hall effect");
              }
                if (line.contains("eo")||line.contains("Eo") || line.contains("\uAB41")){
                 text= text +("\n" +"\uAB41: letter of the Latin alphabet");
              }
                if (line.contains("rge")||line.contains("Rge")){
                 text= text +("\n" +"R-63: Wikimedia disambiguation page");
              }

              if (line.contains("les")||line.contains("Les")){
                 text= text +("\n" +" L-35: proposed fighter aircraft by Albatros");
              }
                if (line.contains("ho")||line.contains("Ho")){
                 text= text +("\n" +" HΦ: chemical element with atomic number 67");
              }
                      if (line.contains("on")||line.contains("On")){
                 text= text +("\n" +" Φπ: province of Canada");
              }
                      if (line.contains("ald")||line.contains("Ald")){
                 text= text +("\n" +" A-LD: scientific article");
              }

                if (line.contains("her")||line.contains("Her")){
                 text= text +("\n" +" H-3R: mammalian protein found in Homo sapiens");
              }
                if (line.contains("hristop")||line.contains("Hristop")){
                 text= text +("\n" +"HRιSTΦP: human settlement in Greece");
              }

  if (line.contains("mas")||line.contains("Mas")){
                 text= text +("\n" +" M-45: highway in Madrid, Spain");
              }
  if (line.contains("an")||line.contains("An")){
                 text= text +("\n" +" λπ: eating disorder characterized by refusal to maintain a healthy body weight, and an obsessive fear of gaining weight due to a distorted self image");
              }

 if (line.contains("iel")||line.contains("Iel")){
                 text= text +("\n" +" I-3L: genus of bacteriophages");
              }


                   if (line.contains("os")||line.contains("Os")){
                 text= text +("\n" +" ΦS: scientific article published on 21 June 2019");
              }
                   if (line.contains("ott")||line.contains("Ott")){
                 text= text +("\n" +" O-77: chemical compound");
              }

                   if (line.contains("eph")||line.contains("Eph")){
                 text= text +("\n" +" E-PH: Australian painter (1865-1915)");
              }


              if (line.contains("har")||line.contains("Hel")){
                  text= text +("\n" +" HλR: Korean family name (하)");
              }

              if (line.contains("ich")||line.contains("Ich")){
                  text= text +("\n" +
                          " ιCH: bleeding within the skull");
              }
              if (line.contains("am")||line.contains("am")){
                  text= text +("\n" +
                          " λ-M: scientific article published on September 2011");
              }


             if (newString.contains("\u03bb")){
                  text= text +("\n" +
                          " λ: spatial period of the wave—the distance over which the wave's shape repeats, and thus the inverse of the spatial frequency");
              }

              if (line.contains("ob")||line.contains("Ob")){
                  text= text +("\n" +
                          " ΦB: scientific article");
              }

 if (line.contains("hn")||line.contains("Hn")){
                  text= text +("\n" +
                          " H-N: scientific journal article");
              }


 if (line.contains("mes")||line.contains("Mes")){
                  text= text +("\n" +
                          " M-35: Open cluster");
              }

             if (line.contains("ill")){
                  text= text +("\n" +
                          " ιLL: river in Elsass");
              }

              if (line.contains("iam")||line.contains("Iam")){
                  text= text +("\n" +
                          " I-4M: scientific article published on 02 December 2019");
              }

              if (line.contains("ard")||line.contains("Ard")){
                  text= text +("\n" +
                          " A-RD: joint organization of Germany's regional public-service broadcasters");
              }

      //        rez.setText(result.toUpperCase());
              rez.setText(result.substring(0,1).toUpperCase()+ result.substring(1));
                t1.setText(text);












          }






          else{
              TastyToast.makeText(getApplicationContext(), "Проверьте количество введенных символов!", TastyToast.LENGTH_LONG, TastyToast.ERROR);

                  }
      }
  });











    }


}